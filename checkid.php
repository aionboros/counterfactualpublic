<?php

include 'config.php'; //We import DB credentials
$ref = $_SERVER['HTTP_REFERER'];

/*
	Function - checkID:
	
	Purpose:
	The checkID function takes a provided PID and checks to see if it can be used, 
	or if it has been already used.
	If no session has been created for that user with that PID, it creates a session.
	Regardless of the result it outputs an echo for Javascript to interpret as response.
*/

function checkID($PPID) {
	$sql = "SELECT * FROM counterfactual WHERE PID = '" . $PPID . "'";
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection error: " . $conn->connect_error);
	}
	$result = $conn->query($sql);
	if ($conn->query($sql) == TRUE) {
		if ($result->num_rows == 0) 
		{
			echo "incorrect";
		}
		/*
			If login is successful, it checks if the provided PID is in use, or if it is available.
			In any other instance (e.g. the PID already submitted data) it terminates the session.
			If the key is not found it reports it as invalid.
		*/
		else if ($result->num_rows == 1) 
		{
			session_name("CounterfactualGame");
			session_start();
			$row = $result->fetch_assoc();
			if ($_SESSION['PID'] == $PPID)
			{
				return "success";
			}
			elseif ($row['date'] == "0000-00-00")
			{
				$_SESSION['PID'] = $PPID;	
				return "success";
			}
			else
			{
				echo "completed";
				session_destroy();
			}
	  	} 
		else 
		{
	     	echo "Key invalid. Request another one from the administrator.";
	   	}
	} else {
		echo "error";
	}
	$conn->close(); 
}

/*
	If the request comes from the right address and has the right POST variable, 
	it sanitizes the input and passes it to the checkID function.
*/
if (strstr($ref,"http://counterfactual.ddns.net") && isset($_POST['PID']) && !isset($_SESSION['PID'])) {
	$PID = filter_input(INPUT_POST, 'PID',FILTER_SANITIZE_STRING);
	echo checkID($PID);
}
?>