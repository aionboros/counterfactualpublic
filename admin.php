<!DOCTYPE html>
<meta charset="UTF-8">
<html>
<head>
<link rel="stylesheet" href="./css/main.css" />
<script src="./js/jquery-2.2.2.min.js"></script>
<script src="./js/sorttable.js"></script>
<script src="./js/table2CSV.js"></script>
<script src="./js/download.js"></script>
<script src="js/sha512.js"></script>

<script>
/* 
Function - crypt()

Purpose:
It hashes the password before it is submitted by the user. That way the password cannot be sniffed in clear text by a packet capture.

Dependencies:
This function uses the jsSHA javascript implementation, found here: 
https://sourceforge.net/projects/jssha/
 
I learned about this script from this thread: 
http://stackoverflow.com/questions/9544159/jquery-jssha-and-sha512-how-to-call-function
*/

function crypt(){
	var password = document.getElementById('password').value;
	var shaObj = new jsSHA("SHA-512", "TEXT");
	shaObj.update(password);
	var hash = shaObj.getHash("HEX");
        document.getElementById('password').value = hash;
}

/*
Function - getCSVData()

Purpose: 
This function transforms the table data into CSV file for download.

Dependencies:
It uses the jQuery utility table2CSV, found here: 
https://github.com/rubo77/table2CSV

Source:
I learned about it from this thread:
http://stackoverflow.com/questions/16376161/javascript-set-file-in-download
*/

function getCSVData(){
	 var csv_value=$('#resulttable').table2CSV({delivery:'download',header:['ID','IP','Date','Event Order','Event Outcome','Event Fault']});
	 download(csv_value,"data.csv","text/csv");
}
</script>

<head>
<body>

<?php
include 'config.php'; //We import DB credentials

/* 
Function -  getGUID()

Use: 
This function creates a GUID. They are used as the registration keys by the game.

Source:
I used this function as presented here:
http://stackoverflow.com/questions/18206851/com-create-guid-function-got-error-on-server-side-but-works-fine-in-local-usin
*/

function getGUID(){
    if (function_exists('com_create_guid')){
        return com_create_guid();
    }
    else {
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12); 
        return $uuid;
    }
}

/*
This next section takes the login POST variables and checks the database to see if there is a username/password match.

Source:
I read a few resources online on how to properly sanitize input and the official PHP docoumentation.  

This thread in particular set me in the right direction:
http://stackoverflow.com/questions/4115719/easy-way-to-password-protect-php-page
*/ 

$username = filter_input(INPUT_POST, 'user',FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST, 'password',FILTER_SANITIZE_STRING);

/* 
Here we make a connection to the database with the details above, or report a connection failure.
*/

$conn = new mysqli($servername, $dbusername, $dbpassword, $dbname);
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

session_name("CounterfactualAdmin");
session_start();

/* 
This section checks for the username and password to be correct through a MySQL query.

If a row is returned, a session stores the username on the server side as an identifier and the page refreshes itself to show the content.
*/

if (isset($_GET['p']) && $_GET['p'] == "login") {
	$sqlusrs = "SELECT * FROM users WHERE user = '" . $username . "' AND password = '" . $password . "'";
	$usrresult = $conn->query($sqlusrs);
	$conn->close();
	if ($usrresult->num_rows == 0) {
		echo "Username or password incorrect.";
		?><br>
		<FORM>
			<INPUT TYPE="button" onClick="window.history.back()" VALUE="Go back">	
		</FORM>
		<?php
		exit;
	}
	else if ($usrresult->num_rows == 1) {
		$_SESSION['username'] = $username;
     	header("Location: $_SERVER[PHP_SELF]");	
  	} else {
     		 echo "Sorry, you could not be logged in at this time.";
   	}
}

/* 
This block checks for the GET variable value that indicates a new participant row should be added.

This GET URL is reached by using one of the buttons in the admin page, listed below.

When executed, it generates a new GUID and adds it in the new row.
*/

if (isset($_GET['p']) && $_GET['p'] == "new") {
	if (isset($_SESSION) && isset($_SESSION['username']))
	{
		$GUID = getGUID();
		$sql = "INSERT IGNORE INTO counterfactual (PID) VALUES ('" . $GUID . "')";
		$result = $conn->query($sql);
		$conn->close();
	        header("Location: $_SERVER[PHP_SELF]");	
	}
}

/*
This block checks if an authenticated session exists, and if it does it checks if the user is trying to log out by seeing the value of the GET variable value.

If the user is trying to log out, the cookie and the session are destroyed.
*/

if (isset($_SESSION['username'])) {
	if (isset($_GET['p']) && $_GET['p'] == "logout") {
		$_SESSION = array();
		if (ini_get("session.use_cookies")) {
			$params = session_get_cookie_params();
   			setcookie(session_name(), '', time() - 42000,
     			$params["path"], $params["domain"],
     			$params["secure"], $params["httponly"]
			);
		}		
		session_destroy();
		header("Location: $_SERVER[PHP_SELF]");
	}
	
?>

<!-- This block displays HTML content -->

	<p>
	<div class="buttonrow">
	<FORM>
		<INPUT TYPE="button" onClick="history.go(0)" VALUE="Refresh">	
		<INPUT TYPE="button" onclick="location.href='?p=logout';" Value="Log out">
		<input type="button" onclick="getCSVData()" value="Download">
		<input type="button" onclick="location.href='?p=new';" value="Create ID">
	</FORM>
	</div>
	</p>	
	<table width=100% id="resulttable" class="sortable">
		<thead>
	    <tr>
	      <th>ID</th>
	      <th>IP</th>
	      <th>Date</th>
	      <th>Order</th>
	      <th>Outcome</th>
	      <th>Fault</th>
	    </tr>
		</thead>
		<tbody>
		<?php
		/*
			This section populates each row of the table with each row in the DB. 
			It instead reads 'No rows returned' if the DB is empty.
		*/
		$sql = "SELECT PID,ipAddr,date,eventOrder,eventOutcome,eventFault FROM counterfactual";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {	
		while($row = $result->fetch_assoc()) {
			echo "<tr><td>{$row["PID"]}</td><td>{$row["ipAddr"]}</td><td>{$row["date"]}</td><td>{$row["eventOrder"]}</td><td>{$row["eventOutcome"]}</td><td>{$row["eventFault"]}</td></tr>\n";
		}
	     	}else{
		       	echo '<tr><td colspan="5">No Rows Returned!</td></tr>';
		}
		$conn->close();
 		?>
		</tbody>
	</table>
	<p style="text-align:center">This site uses cookies. For more information click <a href=cookies.html>here</a>.</p>
<?php
    exit;
}
?>

<!-- This is the login form, shown if the user is not logged in already --> 

<form action="<?php echo $_SERVER['PHP_SELF']; ?>?p=login" method="post">
<table class="loginbox">
	<tr>
		<th colspan="2">Please log in to continue</td>
	</tr>
	<tr>
		<td>Username:</td><td><input type="text" id="user" name="user"></td>
	</tr>
	<tr>
		<td>Password:</td><td><input type="password" id="password" name="password"></td>
	</tr>
	</table>
	<p>
	<div class="buttonrow">
		<input onclick="crypt();" type="submit" id="submit" value="Submit">
	</div>
	</p>
</form>
</body>
<footer>
<p style="text-align:center">This site uses cookies. For more information click <a href=cookies.html>here</a>.</p>
</footer>
</html>