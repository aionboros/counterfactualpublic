<?php
/*
This component receives data submitted by the client, validates it and then stores the input in the database or reports an error if it fails or if it doesn't pass validation. 
*/


include 'config.php'; //We import DB credentials
include 'token.php'; //We import the token generator


// We initialize all variables, sanitize them and store them into variables.

$ref = $_SERVER['HTTP_REFERER'];
$PID = filter_input(INPUT_POST, 'PID',FILTER_SANITIZE_STRING);
$eventOrder = filter_input(INPUT_POST, 'eventOrder',FILTER_SANITIZE_STRING);
$eventOutcome = filter_input(INPUT_POST, 'eventOutcome',FILTER_SANITIZE_STRING);
$eventFault = filter_input(INPUT_POST, 'eventFault',FILTER_SANITIZE_STRING);
$ipAddr= $_SERVER['REMOTE_ADDR']; //This gives us the client’s IP
$date = date("y-m-d");
$sql = "UPDATE counterfactual SET ipAddr = '" . $ipAddr . "', date = '" . $date . "', eventOrder = '" . $eventOrder . "', eventOutcome = '" . $eventOutcome . "', eventFault = '" . $eventFault . "' WHERE PID = '" . $PID . "'";

// Here we define the session, start it, and request a token to add to it.

session_name("CounterfactualGame");
session_start();
$token = getToken();

// If the token is the same we got and if the request came from the right URL, we go ahead.

if ($_SESSION['token'] == $token  && strstr($ref,"http://counterfactual.ddns.net")) {
        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection error: " . $conn->connect_error);
        }

        if ($conn->query($sql) == TRUE) { //We try to update the row

// Then we destroy the client's cookie and session since it is no longer needed. 
            echo "success";
            $_SESSION = array();
            if (ini_get("session.use_cookies")) {
                $params = session_get_cookie_params();
                   setcookie(session_name(), '', time() - 42000,
                     $params["path"], $params["domain"],
                     $params["secure"], $params["httponly"]
                );
            }
            session_destroy();     
        } else {
            echo "Error - Unable to add row. Please contact the site administrator."; // Reports an error if the SQL query failed.
        }
        $conn->close(); 
        } else {
            http_response_code(401); // Return error code if token is invalid.
    }
?>