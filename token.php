<?php
session_name("CounterfactualGame");
session_start();
/*
	This imports the checkID function to use it as part of session validation.
*/
include 'checkid.php';

$ref = $_SERVER['HTTP_REFERER'];

/* 
	Function - getToken():
	
	This function runs a calculation based in current time to produce an unique string. 
	It is issued to the client and to the server scripts at the time of data submission
	in order to validate the source sending the data before it is entered in the database.
	
	The calculation specified in $token can be changed to anything else, as long as it keeps
	at least the $secs variable to ensure that tokens expire every second.
*/

function getToken(){
	$secs = intval(date('s')) + 1;
	$date = intval(date('Ydm'));
	$token = $date * 24122004 * $secs * 24;
	$token = openssl_digest($token,"sha512");
	return $token;
}

/*
	The function is only called if the use has a valid session with a PID number that can be used,
	and if the request came from the right URL.
	
	If it fails to validate the PID, it responds with the error it received from the checkID function.
*/

if (isset($_SESSION['PID']) && strstr($ref,"http://counterfactual.ddns.net"))
{
	$PID = $_SESSION['PID'];
	$result = checkID($PID);
	if ($result == "success")
	{
		$_SESSION['token'] = getToken();
	}
	else
	{
		echo "nope" . $result;
	}
}
?>